<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Carmodel
 *
 * @property int $id
 * @property int $carbrand_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Carbrand|null $carbrand
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel query()
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel whereCarbrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carmodel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Carmodel extends Model
{
    use HasFactory;
	protected $fillable = [
		'carbrand_id',
        'name'
    ];	
	
	public function carbrand(): BelongsTo
		{
		return $this->belongsTo(Carbrand::class);
	}
}
