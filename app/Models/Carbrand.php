<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Carbrand
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand query()
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Carbrand whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Carbrand extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'name'
    ];	
}
