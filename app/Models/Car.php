<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Car
 *
 * @property int $id
 * @property int|null $user_id пользователь автомобиля
 * @property int $carbrand_id марка автомобиля
 * @property int $carmodel_id модель автомобиля
 * @property int|null $year_of_release год выпуска автомобиля
 * @property int $car_mileage пробег автомобиля
 * @property string|null $color цвет автомобиля
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Carbrand|null $carbrand
 * @property-read \App\Models\Carmodel|null $carmodel
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Car newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Car newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Car query()
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereCarMileage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereCarbrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereCarmodelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Car whereYearOfRelease($value)
 * @mixin \Eloquent
 */
class Car extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'carbrand_id',
		'carmodel_id',
		'year_of_release',
		'car_mileage',
		'color'
	];		
	
	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}	
	
	public function carbrand(): BelongsTo
	{
		return $this->belongsTo(Carbrand::class);
	}
	
	public function carmodel(): BelongsTo
	{
		return $this->belongsTo(Carmodel::class);
	}	
	
}
