<?php
namespace App\Repository\Car;

use App\Models\Car;

class Repository {
	
	public function getPaginated(Array $params= [])
	{
		$perPage = $params["per_page"] ?? 16;
		$sortBy = $params["sort_by"] ?? 'uodated_at';
		$order = $params["order"] ?? 'DESC';
		$query = Car::select([
			'*'	
		]);
	
		
		$query->orderBy($sortBy, $order);
		
		
		$collection = $query->paginate($perPage);
		
		return $collection;
	}	
	
    public function getOne(int $id)
	{
		return Car::find($id);
	}
	
	

    public function create(CreateDto $dto) : Car
	{
		
		\DB::beginTransaction();
        try {
		$model = $dto->toModel(); 
			if ($this->save($model)) {
				\DB::commit();
				return $model;
			}
		} catch (QueryException $e) {
            \DB::rollBack();
            throw $e;
        }

        return false;			   
		
	}

    public function update(Car $model, UpdateDto $dto): bool
    {
		\DB::beginTransaction();
        try {
		$dto->toModel($model);
		
			if ($this->save($model)) {
				\DB::commit();
				return true;
			}
		} catch (QueryException $e) {
            \DB::rollBack();
            throw $e;
        }

        return false;			
    }	
	
    private function save(Car $model): bool
    {
            if ($model->save()) {
                \DB::commit();
                return true;
            }
        return false;
    }	
	

    public function delete(Car $model): bool
    {
        return (bool)$model->delete();
    }	

	
}