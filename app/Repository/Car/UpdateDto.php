<?php
declare(strict_types=1);

namespace App\Repository\Car;

use App\Models\Car;
use Illuminate\Support\Facades\Auth;


class UpdateDto extends AbstractDto
{
    public function toModel(Car $model): Car
    {
		
        foreach ($this->attributes as $attribute) {
            if (property_exists($this, $attribute)) {
                 $model->$attribute = $this->$attribute;
            }
        }
		
        return $model;
    }
}
