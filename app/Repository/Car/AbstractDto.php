<?php
namespace App\Repository\Car;

class AbstractDto{
	
    protected array $attributes = [
			'carbrand_id',
			'carmodel_id',
			'year_of_release',
			'car_mileage',
			'color'
    ];	

    public function __construct(array $params = [])
    {
		
        foreach ($this->attributes as $attribute) {
            $keys = array_keys($params);
            if (in_array($attribute, $keys)) {
                $this->{$attribute} = $params[$attribute];
            }
        }

	}
}