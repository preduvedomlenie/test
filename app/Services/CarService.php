<?php
declare(strict_types=1);

namespace App\Services;
use App\Repository\Car\Repository;
use App\Repository\Car\CreateDto;
use App\Repository\Car\UpdateDto;
use App\Models\Car;

class CarService {
	
	private Repository $repo;
	
	public function __construct(
        Repository $repo
		
    ) {
        $this->repo = $repo;
    }
	
    public function create(CreateDto $dto, ?int $userId = null): Car
    {	
			
        $model = $this->repo->create($dto);
		if ($userId) {
			$model->user_id = $userId;
			$model->save();
		}

        return $model;
    }
	
    public function update(Car $model, UpdateDto $dto): bool
    {
		
       $updateResult = $this->repo->update($model, $dto);
	   
       return $updateResult;
    }
	
    public function delete(Car $mopdel): bool
    {
       
        return $this->repo->delete($mopdel);
    }	
	
}	
