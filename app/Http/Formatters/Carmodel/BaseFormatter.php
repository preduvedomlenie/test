<?php  
namespace App\Http\Formatters\Carmodel;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Carmodel;

class BaseFormatter
{
	public function getModel(Carmodel $model) : array
	{
		
		
		$result = [
			"brand_id" => $model->carbrand_id,
			"brand" => $model->carbrand->name ?? '',
			"id" => $model->id,
			"name" => $model->name
		];
		
		return $result;
	}
	
	public function getList(Collection $collection) : array
	{
		$result = [];
		
		foreach ($collection as $model) {
			$result[] = $this->getModel($model);	
		}
		
		return $result;
			 
	}	
}