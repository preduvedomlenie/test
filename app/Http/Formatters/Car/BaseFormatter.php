<?php  
namespace App\Http\Formatters\Car;


use App\Models\Outerresidential;
use App\Models\Car;
use Illuminate\Database\Eloquent\Collection;

class BaseFormatter
{
	public function getModel(Car $model) : array
	{
		
		
		$result = [
			"id" => $model->id,
			"user_id" => $model->user_id,
			"user" => $model->user->name ?? '',
			"brand_id" => $model->carbrand_id,
			"model_id" => $model->carmodel_id,
			"brand" => $model->carbrand->name ?? '',
			"model" => $model->carmodel->name ?? '',
			"year_of_release" => $model->year_of_release,			
			"car_mileage" => $model->car_mileage,			
			"car_mileage" => $model->color,
		];
		
		return $result;
	}
	
	public function getList(Collection $collection) : array
	{
		$result = [];
		
		foreach ($collection as $model) {
			$result[] = $this->getModel($model);	
		}
		
		return $result;
			 
	}	
}