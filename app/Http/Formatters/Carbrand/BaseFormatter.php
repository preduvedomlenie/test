<?php  
namespace App\Http\Formatters\Carbrand;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Carbrand;

class BaseFormatter
{
	public function getModel(Carbrand $model) : array
	{
		
		
		$result = [
			"id" => $model->id,
			"name" => $model->name,
			
		];
		
		return $result;
	}
	
	public function getList(Collection $collection) : array
	{
		$result = [];
		
		foreach ($collection as $model) {
			$result[] = $this->getModel($model);	
		}
		
		return $result;
			 
	}	
}