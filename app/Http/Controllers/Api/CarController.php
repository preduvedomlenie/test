<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Formatters\Car\BaseFormatter;
use App\Models\Car;
use App\Http\Requests\Car\CreateRequest;
use App\Http\Requests\Car\UpdateRequest;
use App\Repository\Car\Repository;
use App\Services\CarService;
use Illuminate\Support\Facades\Gate;


class CarController extends Controller
{
	
	private BaseFormatter $formatter;	
	private CarService $service;	
	private Repository $repo;	
	
	
	public function __construct(
	BaseFormatter $formatter,
	CarService $service,
	Repository $repo
	) {

    		$this->formatter = $formatter;
			$this->service = $service;
			$this->repo = $repo;
    }	
	
/**
 * @OA\Get(
 *     path="/api/cars",
 *     tags={"Cars"},
 *     summary="Получить список автомобилей",
 *     @OA\Response(response="200", description="Автомобили"),
 *   )
 * )
**/
	
	public function index(Request $request)
	{
		$cars = Car::all();
		$result = [
				"data" => $this->formatter->getList($cars)
		];		
				
		return response()->json($result);			
	}
	

/**
 * @OA\Post(
 *     path="/api/cars",
 *     tags={"Cars"},
 *     security={{"Bearer":{}}},
 *     summary="Добавить автомобиль",
 *     @OA\RequestBody(
 *         @OA\JsonContent(),
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 type="object",
 *                 required={"carbrand_id","carmodel_id"},
 *                 @OA\Property(property="carbrand_id",type="integer"),
 *                 @OA\Property(property="carmodel_id",type="integer"),
 *				   @OA\Property(property="year_of_release",type="integer"),
 *				   @OA\Property(property="car_mileage",type="integer"),
 *				   @OA\Property(property="color",type="text"),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="Автомобиль создан",
 *         @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *       response="200",
 *       description="",
 *       @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *         response="422", 
 *         description="Unprocessable Entity",
 *         @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *         response="400",
 *         description="Bad Request",
 *         @OA\JsonContent()
 *     ),
 * )
 * 
**/
	public function store(CreateRequest $request)
	{

		$dto = $request->getDto();
		$car =  $this->service->create($dto, \Auth::user()->id); 
		$car = $this->formatter->getModel($car);
		
		return response()->json($car);			
	}		
	
/**
 * @OA\Put(
 *     path="/api/cars/{id}",
 *     tags={"Cars"},
 *     security={{"Bearer":{}}}, 
 *     summary="Редактировать автомобиль",
 *     @OA\RequestBody(
 *         @OA\JsonContent(),
 *		),
 * 		@OA\Parameter(
 *          name="id",
 *          description="ID автомобиля",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *     ), 
 * 		@OA\Parameter(
 *          name="carbrand_id",
 *          description="ID марки автомобиля",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),  
 * 		@OA\Parameter(
 *          name="carmodel_id",
 *          description="ID мдели автомобиля",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),   
 * 		@OA\Parameter(
 *          name="year_of_release",
 *          description="год выпуска автомобиля",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),   
 * 		@OA\Parameter(
 *          name="car_mileage",
 *          description="пробег автомобиля",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),   
 * 		@OA\Parameter(
 *          name="color",
 *          description="text",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="text"
 *          )
 *      ),   
 *     @OA\Response(
 *         response="201",
 *         description="Автомобиль создан",
 *         @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *       response="200",
 *       description="",
 *       @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *         response="422", 
 *         description="Unprocessable Entity",
 *         @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *         response="400",
 *         description="Bad Request",
 *         @OA\JsonContent()
 *     ),
 * )
 * 
**/
	
	public function update($id, UpdateRequest $request)
	{
		$car = Car::findOrFail($id);
		
		if (!$car) {
			return response()->json(['error' => 'not found'], 404);
		}		
		
		$response = Gate::inspect('update', $car);
		if ($response->allowed()) {
			$dto = $request->getDto();
			
			$this->service->update($car, $dto);
			
			return response()->json($this->formatter->getModel($car));			
		
		} else {
			return response()->json(['error' => $response->message()], 404);	
		
		}


	}	
	
/**	
 * @OA\Delete(
 *     path="/api/cars/{id}",
 *     tags={"Cars"},
 *     security={{"Bearer":{}}}, 
 *     summary="Удалить автомобиль",
 *     @OA\Response(response="204", description="Автмобиль удалён"),
 *     @OA\Parameter(
 *          name="id",
 *          description="ID автмобиля",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *     ),
 *     @OA\Response(
 *         response="422", 
 *         description="Unprocessable Entity",
 *         @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *         response="400",
 *         description="Bad Request",
 *         @OA\JsonContent()
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Bad Request",
 *         @OA\JsonContent()
 *     ), 
 * )
 **/
 
	public function destroy(int $id)
    {
        $car = $this->repo->getOne($id);
		
		if (!$car) {
			return response()->json(['error' => 'not found'], 404);
		}
		$response = Gate::inspect('update', $car);
		if ($response->allowed()) {
			 $deleteResult = $this->service->delete($car);
			if ($deleteResult) {
				return response()->json([], 200);
			}			
		} else {
			
			return response()->json(['error' => $response->message()], 404);	
		
		}		
		
       
		
		return response()->json(['data' => ['is_deleted' => false], 204]);			


    }
}
