<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Formatters\Carmodel\BaseFormatter;
use App\Models\Carmodel;

class CarmodelController extends Controller
{
    private BaseFormatter $formatter;	
	
	public function __construct(BaseFormatter $formatter) {

    		$this->formatter = $formatter;
    }	
	
	/**
	 * @OA\Get(
	 *     path="/api/models",
	 *     tags={"Cars"},
	 *     summary="Получить список моделей автомобилей",
	 *     @OA\Response(response="200", description="Модели"),
	 *   )
	 * )
	**/
	
	
    public function index(Request $request)
	{
		$models = Carmodel::all();
		$result = [
				"data" => $this->formatter->getList($models)
		];		
				
		return response()->json($result);					
		
	}
}
