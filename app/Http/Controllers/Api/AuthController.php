<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;

class AuthController extends Controller
{
/**
* @OA\Post(
*     path="/api/register",
*     operationId="registerUser",
*     tags={"Register"},
*     summary="Register a new user",
*     description="User Registration Endpoint",
*     @OA\RequestBody(
*         @OA\JsonContent(),
*         @OA\MediaType(
*             mediaType="multipart/form-data",
*             @OA\Schema(
*                 type="object",
*                 required={"name","email","password","password_confirmation"},
*                 @OA\Property(property="name",type="text"),
*                 @OA\Property(property="email",type="text"),
*                 @OA\Property(property="password",type="password"),
*                 @OA\Property(property="password_confirmation",type="password"),
*             ),
*         ),
*     ),
*     @OA\Response(
*         response="201",
*         description="User Registered Successfully",
*         @OA\JsonContent()
*     ),
*     @OA\Response(
*       response="200",
*       description="Registered Successfull",
*       @OA\JsonContent()
*     ),
*     @OA\Response(
*         response="422",
*         description="Unprocessable Entity",
*         @OA\JsonContent()
*     ),
*     @OA\Response(
*         response="400",
*         description="Bad Request",
*         @OA\JsonContent()
*     ),
* )
*/


    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }
	//  Login API
	/**
	* @OA\Post(
	*     path="/api/login",
	*     operationId="loginUser",
	*     tags={"Login"},
	*     summary="Login a user",
	*     description="User Login Endpoint",
	*     @OA\RequestBody(
	*         @OA\JsonContent(),
	*         @OA\MediaType(
	*             mediaType="multipart/form-data",
	*             @OA\Schema(
	*                 type="object",
	*                 required={"email","password"},
	*                 @OA\Property(property="email",type="text"),
	*                 @OA\Property(property="password",type="password"),
	*             ),
	*         ),
	*     ),
	*     @OA\Response(
	*         response="201",
	*         description="User Login Successfully",
	*         @OA\JsonContent()
	*     ),
	*     @OA\Response(
	*       response="200",
	*       description="Login Successfull",
	*       @OA\JsonContent()
	*     ),
	*     @OA\Response(
	*         response="422",
	*         description="Unprocessable Entity",
	*         @OA\JsonContent()
	*     ),
	*     @OA\Response(
	*         response="400",
	*         description="Bad Request",
	*         @OA\JsonContent()
	*     ),
	* )
	*/
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('email', 'password');

        $token = Auth::attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized',
            ], 401);
        }

        $user = Auth::user();
        return response()->json([
                'status' => 'success',
                'user' => $user,
                'authorisation' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ]);

    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $token = Auth::login($user);
        return response()->json([
            'status' => 'success',
            'message' => 'User created successfully',
            'user' => $user,
            'authorisation' => [
                'token' => $token,
                'type' => 'bearer',
            ]
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out',
        ]);
    }

    public function refresh()
    {
        return response()->json([
            'status' => 'success',
            'user' => Auth::user(),
            'authorisation' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }

}