<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carbrand;
use App\Http\Formatters\Carbrand\BaseFormatter;

class CarbrandController extends Controller
{
	
	private BaseFormatter $formatter;	
	
	public function __construct(BaseFormatter $formatter) {

    		$this->formatter = $formatter;
    }	
	
	/**
	 * @OA\Get(
	 *     path="/api/brands",
	 *     tags={"Cars"},
	 *     summary="Получить список марок автомобилей",
	 *     @OA\Response(response="200", description="Марки"),
	 *   )
	 * )
	**/
	
	
    public function index(Request $request)
	{
		$brands = Carbrand::all();
		$result = [
				"data" => $this->formatter->getList($brands)
		];		
				
		return response()->json($result);					
		
	}
}
