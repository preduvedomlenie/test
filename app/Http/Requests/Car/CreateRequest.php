<?php
declare(strict_types=1);

namespace App\Http\Requests\Car;
use Illuminate\Validation\Rule;
use App\Repository\Car\CreateDto;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CreateRequest extends AbstractRequest
{

    public function getDto(): CreateDto
    {
        $validated = $this->validated();
		
		$dto = new CreateDto($validated);

        return $dto;
    }	
 
	
}
