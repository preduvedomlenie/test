<?php
declare(strict_types=1);

namespace App\Http\Requests\Car;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class AbstractRequest extends FormRequest
{
	public function rules(): array
    {
		//var_dump($_POST); die();
        return [
			'carbrand_id' => ['required','integer','exists:App\Models\Carbrand,id'],
			'carmodel_id' => ['required','integer','exists:App\Models\Carmodel,id'],
			'year_of_release' => ['nullable','integer'],
			'car_mileage' => ['nullable','integer'],
			'color' => ['nullable','string'],
        ];
    }

	
    public function messages(): array
    {
        return [
	
        ];
    }

}
