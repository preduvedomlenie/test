<?php
declare(strict_types=1);

namespace App\Http\Requests\Car;

use Illuminate\Validation\Rule;
use App\Repository\Car\UpdateDto;



class UpdateRequest extends AbstractRequest
{

    public function getDto(): UpdateDto
    {
        $validated = $this->validated();
		
		$dto = new UpdateDto($validated);

        return $dto;
    }	
	
}