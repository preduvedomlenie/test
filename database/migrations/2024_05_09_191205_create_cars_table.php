<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('user_id')->nullable()->index()->comment('пользователь автомобиля');
			$table->unsignedBigInteger('carbrand_id')->index()->comment('марка автомобиля');
			$table->unsignedBigInteger('carmodel_id')->index()->comment('модель автомобиля');
			$table->integer('year_of_release')->nullable()->index()->comment('год выпуска автомобиля');
			$table->unsignedBigInteger('car_mileage')->default(0)->index()->comment('пробег автомобиля');
			$table->string('color')->index()->nullable()->index()->comment('цвет автомобиля');;
            $table->timestamps();
			
			$table->index(['carbrand_id', 'carmodel_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
};
