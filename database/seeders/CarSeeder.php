<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Carbrand;
use App\Models\Carmodel;
use App\Models\Car;


class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		$brand = Carbrand::create([
			'name' => 'BMW'
		]);	
		
		$model = Carmodel::create([
			'carbrand_id' => $brand->id,
			'name' => 'X3 M'
		]);			
		
		$car = Car::create([
			'carbrand_id' => $brand->id,
			'carmodel_id' => $model->id,
			'year_of_release' => 2010
		]);			
		
		$car = Car::create([
			'carbrand_id' => $brand->id,
			'carmodel_id' => $model->id,
			'year_of_release' => 2013,
			'car_mileage' => 500,
			'color' => 'black'
		]);			
		
		
		$model = Carmodel::create([
			'carbrand_id' => $brand->id,
			'name' => 'X3 M'
		]);					
		
		$model = Carmodel::create([
			'carbrand_id' => $brand->id,
			'name' => '4 Series Gran Coupe'
		]);			
		$model = Carmodel::create([
			'carbrand_id' => $brand->id,
			'name' => 'M6 GRAN COUPE'
		]);			
		
		$brand = Carbrand::create([
			'name' => 'Volvo'
		]);			
		
		$model = Carmodel::create([
			'carbrand_id' => $brand->id,
			'name' => 'V40'
		]);					

		$car = Car::create([
			'carbrand_id' => $brand->id,
			'carmodel_id' => $model->id,
			'year_of_release' => 2017,
			'car_mileage' => 1500,
			'color' => 'red'
		]);				

		$model = Carmodel::create([
			'carbrand_id' => $brand->id,
			'name' => 'XC90'
		]);					
		
		

		
    }
}
