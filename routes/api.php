<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CarbrandController;
use App\Http\Controllers\Api\CarmodelController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::get('brands', [CarbrandController::class, 'index']);
Route::get('models', [CarmodelController::class, 'index']);



	Route::group(
    [
		'prefix' => 'cars',
		'namespace' => 'App\Http\Controllers\Api'
    ],
    static function () {
		Route::get('', 'CarController@index')->name('index');
		Route::group(
		[
			'middleware' => ['jwt'],
		], static function () {
				Route::post('', 'CarController@store');
			});		
			Route::group(
			[
				'prefix' => '{id}',
				'middleware' => ['jwt'],
			], static function () {
				Route::put('', 'CarController@update');
				Route::delete('', 'CarController@destroy');
			});				
	});



